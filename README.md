# Flavour non-universal Z' models

# References

* [Collider Constraints on Z′ Models for Neutral Current B−Anomalies](https://arxiv.org/abs/1904.10954)
* [Naturalising the third family hypercharge model for neutral current BB-anomalies](https://arxiv.org/abs/1905.10327)
* [The role of non-universal Z couplings in explaining the Vus anomaly](https://arxiv.org/abs/2010.12009)
* [Project report from Mustafa](https://gitlab.com/contur/ucl-masters-projects/-/tree/master/2019-20/final-reports)

## Herwig UFO translation

Mustafa's report and the Allanach et al papers above use Third Family Hypercharge Model, see [here](https://gitlab.com/contur/third-family-hypercharge-model) for more on that.

Now (2020) also using a UFO model from Suchita Kulkarni. Some first guidance on using that is here.

First hurdle is Herwig doesn't like the syntax UVCut.real and MZp.real. It gave an errors like:

ufo2peg.converter.PyToCppException: Don't know how to convert MZp module.

This is not an error I've seen before, and looks like something the Herwig UFO interface should really be able to handle. Anyway I replaced UVCut.real with UVCut etc since it's a real parameter anyway, and this meant the ufo2herwig converter ran and made the c++...

##  Herwig Model compilation
---------------------------
Next hurdle... compile/link failed because there is a duplicate module name.

```
FRModel_Vertices_005.o -o FRModel.so
duplicate symbol __ZN6Herwig27describeHerwigFRModelV_V_93E in:
    FRModel_Vertices_001.o
    FRModel_Vertices_005.o
ld: 1 duplicate symbol for architecture x86_64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
make: *** [FRModel.so] Error 1
```

This seems to be because the vertex V_93 is defined twice in vertices.py

Since it looks like the first mention is something you did by hand ("custom UFO pimping"? ) I deleted the second mention from the end of the file - but maybe that isn't the right thing to do? Anyway, then it compiled and linked...

## Herwig run setup
-------------------

Then I set up a Herwig run with gVl1x1 and gVl2x2 both set to 0.5 and g1p set to 1 (the default seems to be 0.2?). Weirdly, this gave zero cross sections. Then I saw that gVl3x3 was defaulted to -1. I set it to zero, and Herwig managed to set up properly.

## Herwig event generation and Rivet run
----------------------------------------

With the above parameters (everything else default, which I think means MZp=1 TeV as specified) SUCCESS! I generated 1000 13TeV events.
The total inclusive Zp cross section is 1.83 fb. There will be a bit of
double counting here between Drell-yan+ISR and Zp+jet production, but that's probably with the LO uncertainties anyway.

The BR Zp-->muons and Zp-->e+e- are both 33%, with nearly all of the rest going to neutrinos.

## Contur run
-------------
I ran contur on the resulting yoda file. If I just use 13 TeV measurements I get 10% exclusion (ie nothing much. It comes from the CM 13 TeV Drell-Yan measurement with very low lumi). If I turn on the ATLAS 13TeV Drell-Yan search (full run 2), I get 100% exclusion. The contur run summary is here:

http://www.hep.ucl.ac.uk/~jmb/Contur/ZP/contur-plots/

I would expect some exclusion from the ATLAS 8TeV DY measurement, I can run some 8 TeV events soon and see. In summary though, some questions...

